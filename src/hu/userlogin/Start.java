package hu.userlogin;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import hu.userlogin.dao.UserDao;
import hu.userlogin.model.User;

public class Start {

	public static void main(String[] args) {
		
		try 
		{
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/user", "root", "1234");
			
			String sha512Pass = getSHA512("titkosjelszó");
			
			User user = new User("user1", "Gipsz", "Gizi", sha512Pass, "user1@pelda.hu");
			
			
			//String sha512Pass = getSHA512("titkosjelszó2");
			
			//User user = new User("user2", "Marton", "Jakab", sha512Pass, "user2@pelda.hu");
			
			UserDao dao = new UserDao(conn);
			dao.create(user);
			int i = 1;
			System.out.println("Teszt");
			while(dao.read(i)!=null) {
				System.out.println(i);
				if (dao.read(i).getPassword().equals(getSHA512("titkosjelszó"))) {
				System.out.println(i + " Megvan!!!");
				}
				i++;
			}
			
			
			
		} 
		catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}
	
	private static String getSHA512(String password) throws NoSuchAlgorithmException {
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
		//salt megnézése
		
		byte[] result = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
		
		StringBuilder sb = new StringBuilder();
        for(int i=0; i< result.length ;i++){
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
		
        return sb.toString();
	}

}
